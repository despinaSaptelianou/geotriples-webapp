package gr.uoa.di.geotriples.model;

import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import gr.uoa.di.geotriples.FileUploadController;
import gr.uoa.di.geotriples.GeoTriplesController;
import gr.uoa.di.geotriples.configuration.SupportedDataTypes;

public class FileDetails {
	public FileDetails(String filename) {
		this.filename = filename;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getLink() {
		return MvcUriComponentsBuilder.fromMethodName(FileUploadController.class, "serveFile", filename).build()
				.toString();
	}

	public String getGenerateMappingLink() {
		int i = filename.lastIndexOf('.');
		String extension = "";
		if (i > 0) {
			extension = filename.substring(i + 1);
			SupportedDataTypes dt = null;
			try {
				dt = SupportedDataTypes.valueOf(extension);
			} catch (IllegalArgumentException e) {
				return null;
			} catch (NullPointerException e) {
				return null;
			}
			return MvcUriComponentsBuilder.fromMethodName(GeoTriplesController.class, "generateMapping", null, filename)
					.build().toString();
		} else {
			return null;
		}
	}
	
	public String getType() {
		int i = filename.lastIndexOf('.');
		String extension = "";
		if (i > 0) {
			extension = filename.substring(i + 1);
			SupportedDataTypes dt = null;
			try {
				dt = SupportedDataTypes.valueOf(extension);
				return "/images/"+dt.toString();
			} catch (IllegalArgumentException e) {
				return "/images/other";
			} catch (NullPointerException e) {
				return "/images/other";
			}
		} else {
			return "/images/other";
		}
	}

	public String getDeleteLink() {
		return MvcUriComponentsBuilder.fromMethodName(FileUploadController.class, "deleteFile", filename, null).build()
				.toString();
	}

	String filename;
	String link;
	String generateMappingLink;
	String deleteLink;
	String type;
	
}
