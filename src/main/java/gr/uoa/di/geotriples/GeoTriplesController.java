package gr.uoa.di.geotriples;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openrdf.model.impl.URIImpl;
import org.openrdf.rio.RDFFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import be.ugent.mmlab.rml.core.RMLEngine;
import be.ugent.mmlab.rml.core.RMLMappingFactory;
import be.ugent.mmlab.rml.function.Config;
import be.ugent.mmlab.rml.function.FunctionArea;
import be.ugent.mmlab.rml.function.FunctionAsGML;
import be.ugent.mmlab.rml.function.FunctionAsWKT;
import be.ugent.mmlab.rml.function.FunctionCentroidX;
import be.ugent.mmlab.rml.function.FunctionCentroidY;
import be.ugent.mmlab.rml.function.FunctionCoordinateDimension;
import be.ugent.mmlab.rml.function.FunctionDimension;
import be.ugent.mmlab.rml.function.FunctionEQUI;
import be.ugent.mmlab.rml.function.FunctionFactory;
import be.ugent.mmlab.rml.function.FunctionHasSerialization;
import be.ugent.mmlab.rml.function.FunctionIs3D;
import be.ugent.mmlab.rml.function.FunctionIsEmpty;
import be.ugent.mmlab.rml.function.FunctionIsSimple;
import be.ugent.mmlab.rml.function.FunctionLength;
import be.ugent.mmlab.rml.function.FunctionSpatialDimension;
import be.ugent.mmlab.rml.model.RMLMapping;
import eu.linkedeodata.geotriples.GeoTriplesCMD;
import gr.uoa.di.geotriples.model.FileDetails;
import gr.uoa.di.geotriples.storage.StorageFileNotFoundException;
import gr.uoa.di.geotriples.storage.StorageService;
import net.antidot.semantic.rdf.model.impl.sesame.SesameDataSet;

@Controller
public class GeoTriplesController {

	private final StorageService storageService;

	@Autowired
	public GeoTriplesController(StorageService storageService) {
		this.storageService = storageService;
	}

	@GetMapping("/generateMapping/{filename:.+}")
	public String generateMapping(Model model, @PathVariable String filename) throws Exception {
		Resource file = storageService.loadAsResource(filename);
		if (new FileDetails(file.getFilename()).getGenerateMappingLink() == null) {
			throw new UnsupportedDataType(
					"Datatype " + file.getFilename() + " is not supported. Supported types are: csv, xml, shp, json");
		}
		String mapping_file = "mapping_" + filename + ".ttl";
		try {
			GeoTriplesCMD.main(new String[] { "generate_mapping", "-rml", "-o", mapping_file, "-b",
					"http://example.com", file.getFile().getAbsolutePath() });
		} catch (Exception e) {
			throw new ErrorInMappingGeneration();
		}
		String mapping = new Scanner(new File(mapping_file)).useDelimiter("\\Z").next();
		String rx = "(?<=rml:source)\\s+([^;]*)";

		StringBuffer sb = new StringBuffer();
		Pattern p = Pattern.compile(rx);
		Matcher m = p.matcher(mapping);

		while (m.find()) {
			// Avoids throwing a NullPointerException in the case that you
			// Don't have a replacement defined in the map for the match

			System.out.println(m.group().trim());
			String candidate = m.group().trim().replaceAll("\"", "");
			candidate = new File(candidate).getName();
			m.appendReplacement(sb, " \"" + candidate + "\"");
		}
		m.appendTail(sb);

		model.addAttribute("mapping", sb.toString());
		return "generate_mapping";
	}

	public static void registerFunctions() {
		FunctionFactory.registerFunction(new URIImpl("http://www.w3.org/ns/r2rml-ext/functions/def/equi"),
				new FunctionEQUI()); // don't remove or change this line, it
										// replaces the equi join functionality
										// of R2RML

		FunctionFactory.registerFunction(new URIImpl("http://www.w3.org/ns/r2rml-ext/functions/def/asWKT"),
				new FunctionAsWKT());
		FunctionFactory.registerFunction(new URIImpl("http://www.w3.org/ns/r2rml-ext/functions/def/hasSerialization"),
				new FunctionHasSerialization());
		FunctionFactory.registerFunction(new URIImpl("http://www.w3.org/ns/r2rml-ext/functions/def/asGML"),
				new FunctionAsGML());
		FunctionFactory.registerFunction(new URIImpl("http://www.w3.org/ns/r2rml-ext/functions/def/isSimple"),
				new FunctionIsSimple());
		FunctionFactory.registerFunction(new URIImpl("http://www.w3.org/ns/r2rml-ext/functions/def/isEmpty"),
				new FunctionIsEmpty());
		FunctionFactory.registerFunction(new URIImpl("http://www.w3.org/ns/r2rml-ext/functions/def/is3D"),
				new FunctionIs3D());
		FunctionFactory.registerFunction(new URIImpl("http://www.w3.org/ns/r2rml-ext/functions/def/spatialDimension"),
				new FunctionSpatialDimension());
		FunctionFactory.registerFunction(new URIImpl("http://www.w3.org/ns/r2rml-ext/functions/def/dimension"),
				new FunctionDimension());
		FunctionFactory.registerFunction(
				new URIImpl("http://www.w3.org/ns/r2rml-ext/functions/def/coordinateDimension"),
				new FunctionCoordinateDimension());
		FunctionFactory.registerFunction(new URIImpl("http://www.w3.org/ns/r2rml-ext/functions/def/area"),
				new FunctionArea());
		FunctionFactory.registerFunction(new URIImpl("http://www.w3.org/ns/r2rml-ext/functions/def/length"),
				new FunctionLength());
		FunctionFactory.registerFunction(new URIImpl("http://www.w3.org/ns/r2rml-ext/functions/def/centroidx"),
				new FunctionCentroidX());
		FunctionFactory.registerFunction(new URIImpl("http://www.w3.org/ns/r2rml-ext/functions/def/centroidy"),
				new FunctionCentroidY());

	}

	static {
		registerFunctions();
	}

	@PostMapping("/dumpRdf")
	@ResponseBody
	public ResponseEntity<Resource> dumpRDF(Model model, String mapping) throws Exception {
		//
		System.out.println(mapping);
		// String line ="${env1}sojods${env2}${env3}";
		String rx = "(?<=rml:source)\\s+([^;]*)";

		StringBuffer sb = new StringBuffer();
		Pattern p = Pattern.compile(rx);
		Matcher m = p.matcher(mapping);

		while (m.find()) {
			// Avoids throwing a NullPointerException in the case that you
			// Don't have a replacement defined in the map for the match

			System.out.println(m.group().trim());
			String filename = m.group().trim().replaceAll("\"", "");
			if (filename.startsWith("/")) {
				throw new FilenameStartsWithSlash();
			}
			Resource file = storageService.loadAsResource(filename);
			m.appendReplacement(sb, " \"" + file.getFile().getAbsolutePath() + "\"");
		}
		m.appendTail(sb);

		System.out.println(sb.toString());
		// if (true) {
		// return ResponseEntity.ok().body(null);
		// }

		// mapping.replaceAll("rml:source", replacement)
		InputStream is = new ByteArrayInputStream(sb.toString().getBytes(StandardCharsets.UTF_8));
		RMLMapping mapping_object = RMLMappingFactory.extractRMLMapping(is); // edw
																				// einai
																				// to
																				// mapping
																				// file
		Config.EPSG_CODE = "4326";

		// mapping.replaceAll("rml:source", replacement)
		RDFFormat format = RDFFormat.NTRIPLES;

		RMLEngine engine = new RMLEngine();
		SesameDataSet triples = engine.runRMLMapping(mapping_object, "TestGraph", format);
		triples.dumpRDF("output.txt", format);

		Resource output = new UrlResource((Paths.get("output.txt")).toUri());
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + output.getFilename() + "\"")
				.body(output);
	}

	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}

	@ExceptionHandler(ErrorInMappingGeneration.class)
	public ResponseEntity handleErrorInMappingGeneration(ErrorInMappingGeneration exc) {
		return ResponseEntity.unprocessableEntity().build();
	}

	@ExceptionHandler(FilenameStartsWithSlash.class)
	public ResponseEntity handleFilenameStartsWithSlash(FilenameStartsWithSlash exc) {
		return ResponseEntity.unprocessableEntity().build();
	}

	@ExceptionHandler(UnsupportedDataType.class)
	public String handleUnsupportedDataType(UnsupportedDataType exc,RedirectAttributes redirectAttributes) {
		if (redirectAttributes != null){
			redirectAttributes.addFlashAttribute("message", exc.getMessage() + " :(");
			System.out.println("Adding redirect attributes....");
		}
		return "redirect:/";
	}

}
