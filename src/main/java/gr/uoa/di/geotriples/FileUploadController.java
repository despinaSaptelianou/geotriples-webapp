package gr.uoa.di.geotriples;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import gr.uoa.di.geotriples.model.FileDetails;
import gr.uoa.di.geotriples.storage.StorageFileNotFoundException;
import gr.uoa.di.geotriples.storage.StorageService;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.stream.Collectors;

@Controller
public class FileUploadController {

	private final StorageService storageService;

	@Autowired
	public FileUploadController(StorageService storageService) {
		this.storageService = storageService;
	}

	@GetMapping("/")
	public String listUploadedFiles(Model model) throws IOException {

		model.addAttribute("files", storageService.loadAll().map(path -> new FileDetails(path.getFileName().toString()))
				.collect(Collectors.toList()));
		return "uploadForm";
	}

	@GetMapping("/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

		Resource file = storageService.loadAsResource(filename);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}

	@PostMapping("/files/delete/{filename:.+}")
	public String deleteFile(@PathVariable String filename, RedirectAttributes redirectAttributes) {
		Resource file = storageService.loadAsResource(filename);
		try {
			if (file.getFile().delete()) {
				redirectAttributes.addFlashAttribute("message",
						"You successfully deleted " + file.getFilename() + "!");

			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("message",
					"Error in deleting " + file.getFilename() + "!");
		}
		return "redirect:/";

	}

	@PostMapping("/")
	public String handleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {

		storageService.store(file);
		redirectAttributes.addFlashAttribute("message",
				"You successfully uploaded " + file.getOriginalFilename() + "!");

		return "redirect:/";
	}

	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}

}
