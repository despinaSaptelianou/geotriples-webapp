package gr.uoa.di.geotriples.configuration;

public enum SupportedDataTypes {
	csv("csv"), shp("shp"), xml("xml"), json("json");

	private final String text;

	/**
	 * @param text
	 */
	private SupportedDataTypes(final String text) {
		this.text = text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}
}
