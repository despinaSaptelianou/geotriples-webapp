package gr.uoa.di.geotriples;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class UnsupportedDataType extends Exception {
	private RedirectAttributes redirectAttributes=null;

	public UnsupportedDataType(String message) {
		super(message);
	}
	public UnsupportedDataType(String message,RedirectAttributes redirectAttributes) {
		super(message);
		this.setRedirectAttributes(redirectAttributes);
	}
	public RedirectAttributes getRedirectAttributes() {
		return redirectAttributes;
	}
	public void setRedirectAttributes(RedirectAttributes redirectAttributes) {
		this.redirectAttributes = redirectAttributes;
	}
}
